from __future__ import unicode_literals

from django.db import models
import datetime

# Create your models here.

class Occupation(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    created_at = models.DateTimeField(editable=False)
    updated_at = models.DateTimeField(editable=False)

    def __str__(self):
        return self.name
    
    class Meta(object):
        verbose_name = 'Cargo'
        verbose_name_plural = 'Cargos'
    
    def save(self):
        if not self.id:
            self.created_at = datetime.datetime.today()
        self.updated_at = datetime.datetime.today()
        super(Occupation, self).save()  